#!/usr/bin/env python
#encoding: utf - 8
"""
@author: Muhammad Samiullah Ilyas
"""
from pymongo import MongoClient
import tweepy
from geopy.geocoders import Nominatim
import reverse_geocoder as rg
import time
from tweepy import Stream
from tweepy import OAuthHandler
from tweepy.streaming import StreamListener
import json
import re
import pytz
from .sentiment_analysis import calculate_sentiment
from . import views as v
import datetime
import codecs
import os
import sys
from project.models import tokens,hourlycount,lastcount,tweets

geolocator = Nominatim(user_agent="Golden")
__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
INPUT_DIR = os.path.join(__location__, 'Stream_Generators.txt')
generators =[]

file = codecs.open(INPUT_DIR,"r",encoding='utf-8-sig')
for generator in file: 
    gen = " ".join(generator.split())
    generators.append(gen)
client = MongoClient('mongodb://localhost:27017/')

class MyStream(tweepy.Stream):

    def __init__(self,auth,listener):
        super().__init__(auth=auth,listener=listener)

    def _read_loop(self, resp):
      charset = resp.headers.get('content-type', default='')
      enc_search = re.search(r'charset=(?P<enc>\S*)', charset)
      if enc_search is not None:
            encoding = enc_search.group('enc')
      else:
            encoding = 'utf-8'
      buf = tweepy.streaming.ReadBuffer(resp.raw, self.chunk_size, encoding=encoding)
      name=self.listener.country
      while self.running and not resp.raw.closed:
            if datetime.datetime.now()>self.listener.refresh:
                     self.running=False
                     break
# Check if the corresponding event is set or not. If set then close the stream
            if  v.list[name].is_set():
                self.running=False
                break

            length = 0
            while not resp.raw.closed:
                line = buf.read_line()
                stripped_line = line.strip() if line else line # line is sometimes None so we need to check here
                if not stripped_line:
                    self.listener.keep_alive()  # keep-alive new lines are expected
                elif stripped_line.isdigit():
                    length = int(stripped_line)
                    break
                else:
                    raise TweepError('Expecting length, unexpected value found')

            next_status_obj = buf.read_len(length)
            if self.running and next_status_obj:
                self._data(next_status_obj)
      if resp.raw.closed:
            self.on_closed(resp)


# Inherit from the StreamListener object
class MyStreamListener(tweepy.StreamListener):
    db = client.project
    country=None
    access=None
    capital=None
    a=True
    refresh=None
    def __init__(self):
        super(MyStreamListener, self).__init__()
#Called once connected to streaming server
    def on_connect(self): 
        print("You are now connected to the streaming API.")

# Error handling
    def on_error(self, status_code): 
        str='An Error has occured: ' + repr(status_code) + " "+datetime.datetime.now().strftime("%m/%d/%Y, %H:%M:%S")
        self.db.project_tokens.update_one({"access_token":self.access},{"$set":{"last_error":str}})
        time.sleep(10)
        return False
    
    def on_exception(self, exception):
        str = 'An Exception has occured: ' + repr(exception) + " "+datetime.datetime.now().strftime("%m/%d/%Y, %H:%M:%S")
        self.db.project_tokens.update_one({"access_token":self.access},{"$set":{"last_error":str}})
        return False

# Timeout handling
    def on_timeout(self):
         # Print timeout message
        str= 'Timeout...'+datetime.datetime.now().strftime("%m/%d/%Y, %H:%M:%S")
        # Wait 10 sec
        time.sleep(10)
        # Return nothing
        self.db.project_tokens.update_one({"access_token":access_token},{"$set":{"last_error":str}})
        return True
    
#Overload the on_data method
    def on_data(self, data):
        try:
            self.a=True
            json_data1={}
            json_data = json.loads(data)
            
# Check if the data is a tweet or not
            if 'delete' and 'event'and 'direct_message'and 'friends' and 'limit' and 'disconnect' and 'warning' not in json_data:                         
# Check if location is given or not. If location is given then keywords parameter is set. Check if the tweet contains the required keywords
                  if json_data['coordinates']:
                            coordinates = (json_data['coordinates']['coordinates'][1],json_data['coordinates']['coordinates'][0])
                            try:
                                results = rg.search(coordinates)
                                if results[0]['cc'] != self.country:
                                     self.a=False
                            except:
                                pass
                  elif json_data['place']:
                            if json_data['place']['country_code'] != self.country:
                                     self.a=False
                  if json_data['truncated']:
                        if json_data['extended_tweet']['entities']['urls']:
                            for url in json_data['extended_tweet']['entities']['urls']:
                                if any(generator in url['display_url']  for generator in generators):
                                         self.a=False
                                         break
                  elif  json_data['entities']['urls']:
                            for url in json_data['entities']['urls']:
                                if any(generator in url['display_url']  for generator in generators):
                                         self.a=False
                                         break
# Calculate each tweet user's bot probability and see if tweets contained the required keywords or nor if location is specified
                  if(self.a is True):
# Calculate sentiment and also get cleaned text. If quoted a retweet then original otherwise retweet. If a tweet is a retweet then text is of retweet 
# and sentiment is also calculated from retweet. Same applies for quote. If retweet is of quote then original not retweet is taken for sentiment analysis

                    if json_data["retweeted"] is True:
                        if json_data['retweeted_status']["is_quote_status"]:
                             if json_data['retweeted_status']['truncated'] is True: #(1) added comment tweet
                                 text,sentiment=calculate_sentiment(json_data['lang'],json_data['retweeted_status']['extended_tweet']['full_text'])
                                 full_text=json_data['retweeted_status']['extended_tweet']['full_text']
                             else:
                                 text,sentiment=calculate_sentiment(json_data['lang'],json_data['retweeted_status']['text'])
                                 full_text=json_data['retweeted_status']['text']
                        else:
                             if json_data['truncated'] is True: #(1) added comment tweet
                                 text,sentiment=calculate_sentiment(json_data['lang'],json_data['extended_tweet']['full_text'])
                                 full_text=json_data['extended_tweet']['full_text']
                             else:
                                 text,sentiment=calculate_sentiment(json_data['lang'],json_data['text'])
                                 full_text=json_data['text']
                    elif json_data['is_quote_status'] is True and "quoted_status" in json_data:
                        if json_data['quoted_status']["retweeted"]:
                             if json_data['quoted_status']['truncated'] is True: #(1) added comment tweet
                                 text,sentiment=calculate_sentiment(json_data['lang'],json_data['quoted_status']['extended_tweet']['full_text'])
                                 full_text=json_data['quoted_status']['extended_tweet']['full_text']
                             else:
                                 text,sentiment=calculate_sentiment(json_data['lang'],json_data['quoted_status']['text'])
                                 full_text=json_data['quoted_status']['text']
                        else:
                             if json_data['truncated'] is True: #(1) added comment tweet
                                text,sentiment=calculate_sentiment(json_data['lang'],json_data['extended_tweet']['full_text'])
                                full_text=json_data['extended_tweet']['full_text']
                             else:
                                text,sentiment=calculate_sentiment(json_data['lang'],json_data['text'])
                                full_text=json_data['text']
                    else:
                        if json_data['truncated'] is True: #(1) added comment tweet
                            text,sentiment=calculate_sentiment(json_data['lang'],json_data['extended_tweet']['full_text'])
                            full_text=json_data['extended_tweet']['full_text']
                        else:
                            text,sentiment=calculate_sentiment(json_data['lang'],json_data['text'])
                            full_text=json_data['text']

# Save the parameters that are required for visualizing the data. Some of the data are used by the network analysis tool. Their name and structure
# are used in the way so that they can be used in the network analysis tool. Hashtags and mentions are list of strings. Type is entered so that tweets
# can be categorized in the tweet feed accordingly.   
                    json_data1['sentiment_analysis']=sentiment	
                    json_data1['country']=self.country
                    json_data1["text"]=full_text
                    json_data1["cleaned_text"]=text.split(" ")
                    json_data1['status_id']=json_data['id_str']
                    json_data1["hashtags"]=[]
                    json_data1["mentions_screen_name"]=[]
                    json_data1["retweet_screen_name"]=[]
                    json_data1["quoted_screen_name"]=[]
# Saving the hastags, mentions involved in the tweet. If retweet then hashtags and mentions will also be included in the retweet status, Same applies for
# Quoted tweet
                    if json_data["retweeted"] is True:
                        for hashtags in json_data['retweeted_status']['entities']['hashtags']:
                                 json_data1["hashtags"].append(hashtags["text"])
                        for mentions in json_data['retweeted_status']['entities']['user_mentions']:
                                     json_data1["mentions_screen_name"].append(mentions["screen_name"])

                    if json_data['is_quote_status'] is True and 'quoted_status' in json_data:
                        for hashtags in json_data['quoted_status']['entities']['hashtags']:
                                 json_data1["hashtags"].append(hashtags["text"])
                        for mentions in json_data['quoted_status']['entities']['user_mentions']:
                                 json_data1["mentions_screen_name"].append(mentions["screen_name"])

                    if json_data['truncated'] is True:
                        for hashtags in json_data['extended_tweet']['entities']['hashtags']:
                                 json_data1["hashtags"].append(hashtags["text"])
                        for mentions in json_data['extended_tweet']['entities']['user_mentions']:
                                 json_data1["mentions_screen_name"].append(mentions["screen_name"])
                       
                    for hashtags in json_data['entities']['hashtags']:
                             json_data1["hashtags"].append(hashtags["text"])
                    for mentions in json_data['entities']['user_mentions']:
                             json_data1["mentions_screen_name"].append(mentions["screen_name"])

                    if "media" in json_data['entities']:
                        for media in json_data['entities']['media']:
                            if media['type']=='photo' or media['type']=='video':
                                 json_data1["type"]=media['type']
                            else:
                                 json_data1['type']="other"
                    elif json_data['truncated'] is True:
                       if len(json_data['extended_tweet']['entities']['urls']):
                        for urls in json_data['extended_tweet']['entities']['urls']:
                             json_data1["type"]="Links"                        
                       if len(json_data['entities']['urls']):
                        for urls in json_data['entities']['urls']:
                             json_data1["type"]="Links"                        					
                    elif len(json_data['entities']['urls']):
                        for urls in json_data['entities']['urls']:
                             json_data1["type"]="Links"                        
                    else:
                        json_data1['type']="other"						                    
                   
                    if json_data["retweeted"] is True:
                           json_data1["retweet_screen_name"]=json_data["retweeted_status"]["user"]["screen_name"]
                    if json_data["is_quote_status"] is True and 'quoted_status' in json_data:
                           json_data1["quoted_screen_name"]=json_data["quoted_status"]["user"]["screen_name"]

                    
                    if(len(json_data1["retweet_screen_name"])==0):
                            json_data1["retweet_screen_name"]=""
                    
                    if(len(json_data1["quoted_screen_name"])==0):
                            json_data1["quoted_screen_name"]=""   
                    if(json_data["coordinates"]):
                        json_data1["coordinates"]=[]       
                        json_data1["coordinates"].extend([json_data["coordinates"]["coordinates"][0],json_data["coordinates"]["coordinates"][1]]) 
                    elif(json_data["place"]):
                        try:
                            location=geolocator.geocode(json_data["place"]["full_name"])
                            json_data1["coordinates"]=[location.longitude,location.latitude]
                        except:
                            location=self.capital.split(",")
                            location=[float(x) for x in location]
                            json_data1["coordinates"]=[location[0],location[1]]
                    else:
                        location=self.capital.split(",")
                        location=[float(x) for x in location]
                        json_data1["coordinates"]=[location[0],location[1]]
                    json_data1["is_retweet"]=json_data["retweeted"]
                    json_data1['screen_name']=json_data['user']['screen_name']
                    json_data1["is_quote"]=json_data["is_quote_status"]
                    json_data1['text']=json_data["text"]
                    json_data1['retweet_count']=json_data['retweet_count']
# Converting dates to saudd time
                    utc=datetime.datetime.strptime(json_data['created_at'],'%a %b %d %H:%M:%S +0000 %Y') 
                    tzz=pytz.timezone('Asia/Riyadh')
                    utc = utc.replace(tzinfo=pytz.utc).astimezone(tzz)
                    json_data1['created_at']=utc
                    json_data1['timestamp_ms']=(int(int(json_data['timestamp_ms'])/1000)+10800)*1000
# This is used to create an entry in the index of elasticsearch. This will be used for searching inside the projects
                    t=tweets(country=json_data1['country'],text=json_data1['text'],cleaned_text=json_data1["cleaned_text"],hashtags=json_data1["hashtags"],
	            retweet_screen_name=json_data1['retweet_screen_name'],mentions_screen_name=json_data1["mentions_screen_name"],quoted_screen_name=json_data1['quoted_screen_name'],
        	    sentiment_analysis=json_data1['sentiment_analysis'],status_id=json_data1['status_id'],profile_image_url_https=json_data["user"]["profile_image_url_https"],
          	    is_retweet=json_data1['is_retweet'],is_quote=json_data1['is_quote'],created_at=json_data1['created_at'],timestamp_ms=json_data1['timestamp_ms'],screen_name=json_data1['screen_name'],
                    coordinates=json_data1['coordinates'],retweet_count=json_data1['retweet_count'],type=json_data1['type'],topics=[])
                    t.save()
                    time=self.db.project_lastcount.find_one({"access_token":self.access})
                    if time:
                        now = datetime.datetime.now()
                        if time['Time']+datetime.timedelta(hours=1)<now:
                           t={}
                           t['count']=time['count']
                           t['Time']=time['Time']
                           t['access_token']=time['access_token']
                           self.db.project_hourlycount.insert(t)
                           now=datetime.datetime.now(datetime.timezone.utc)
                           Time=now.replace(minute=0,second=0)
                           self.db.project_lastcount.update_one({"access_token":time['access_token']},{"$set":{"count":0,"Time":Time}})
                        else:
                           self.db.project_lastcount.update_one({"access_token":self.access},{"$set":{"count":time["count"]+1}})
                    else:
                        now = datetime.datetime.now(datetime.timezone.utc)
                        Time=now.replace(minute=0,second=0)
                        jjson={}
                        jjson['Time']=Time
                        jjson['count']=1
                        jjson['access_token']=self.access
                        self.db.project_lastcount.insert(jjson)
            return True

        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            st='An Error has occured: '+datetime.datetime.now().strftime("%m/%d/%Y, %H:%M:%S")+" "+str(e)
            self.db.project_tokens.update_one({"access_token":self.access},{"$set":{"last_error":st}})
            return True
# Starting the stream
def start_stream(stream,country,coordinates,access,capital):
    try :
        stream.listener.country=country
        stream.listener.access=access
        stream.listener.refresh=datetime.datetime.now()+datetime.timedelta(hours=12)
        stream.listener.capital=capital
# Check if coordinates are given or not. If yes then place the keywords inside the listener objects keyword variable
        stream.filter(locations=coordinates,is_async=False)
    except Exception as e:
        stream.disconnect()
        time.sleep(10)
# Starting the projects. First see if there is a token that is being used by only 1 project. If not then search for a new token        
def start_project(country,coordinates,capital):
    db1 = client.project
    token=db1.project_tokens.find_and_modify({"running":False},{"$set":{"running":True}})
    if not token:
            return
    access_token=token['access_token']
# Change the running parameters of the token accessed
#    db1.project_tokens.update_one({"access_token":token['access_token']},{"$set":{"running":True}})	
# Variables that contains the user credentials to access Twitter API
    ACCESS_TOKEN = token["access_token"]
    ACCESS_TOKEN_SECRET = token["access_key"]
    CONSUMER_KEY = token["consumer_key"]
    CONSUMER_SECRET = token["consumer_secret"]

#create an OAuthHandler instance and pass the consumer token and secret 
    auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
    auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)
# Create a wrapper for the API provided by Twitter
    api = tweepy.API(auth,wait_on_rate_limit=True,wait_on_rate_limit_notify=True)
    # Add your wrapper and your listener to the stream object
    mylistener=MyStreamListener()
    myStream = MyStream(auth=api.auth, listener=mylistener)
    start_stream(myStream,country,coordinates,access_token,capital) 
# Change the running parameters of the token so that it can account for a new project
    db1.project_tokens.update_one({"access_token":token['access_token']},{"$set":{"running":False}})
