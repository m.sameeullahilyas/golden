This directory contains files that are responsible for handling the project related functions like running, stopping, edit etc. The main files 
for this directory are:

1) views.py:
           Contains functions that define the starting, stopping, edit, creation and other project related functions. Each new project created has a unique 
id and this unique id is used for stopping a project and also for identifying whether a project is running or not. Each project that is run will create a 
new thread with a unique name equal to project<id> where id is the unique parameter of each project. Each tweet will have a field of project id which
will show under which project this tweet belongs to. Also each bot user will have a project id which will show under which project this bot user is associated with

2) models.py:
            Defines the fields that will be stored for each project in the Db. Id is a unique field that is self incrementing. Also defines the model for tokens
Each token has a running parameter which shows how many projects are using this token. This serves as a check for limiting number of project under each
token.

3) Stream.py:
            File that is used to collect stream. For multiple tokens running parameter is used. If running is 2 then token cannot be used otherwise it can belongs
assigned to a project. On exceptions thread is slept for 10 seconds and on error if stream is closed and user has not stopped the project, the stream will be
conected again.

4) sentiment_analysis.py:
             This file is used to calculate the sentiment score of the tweet that is being collected.

5) Proprocessor.py:
             This file is used to preprocess the tweets and then the cleaned text is used to calculate the sentiment. Also engtext function is used to remove
stopwords from the tweets and then the cleaned text is stored as a cleaned text parameter in the Db 

6) Stopwords.py:
             Generates a list of stopwords that are to be removed from the tweets

7) admin.py:
            Contains the configuration for the project and tokens model that are displayed in the admin panel

The template files used are:

1) index.html
2) edit.html
3) account_edit.html			