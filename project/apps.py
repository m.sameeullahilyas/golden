
from django.apps import AppConfig
class ProjectConfig(AppConfig):
    name = 'project'
    def ready(self):
        from . import views as v
        from .models import stream
        from threading import Event
        running_countries=stream.objects.filter(running=True)
        for running_country in running_countries:
            event=Event()
            print("asd")
            name=running_country.country
            v.list[name]=event;
            v.start_new_project(running_country.country,running_country.bounding_box,running_country.capital)

