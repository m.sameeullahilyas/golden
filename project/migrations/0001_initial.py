# Generated by Django 2.2.3 on 2019-09-20 20:36

from django.db import migrations, models
import djongo.models.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='hourlycount',
            fields=[
                ('_id', djongo.models.fields.ObjectIdField(auto_created=True, primary_key=True, serialize=False)),
                ('count', models.IntegerField()),
                ('Time', models.DateTimeField()),
                ('access_token', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='lastcount',
            fields=[
                ('_id', djongo.models.fields.ObjectIdField(auto_created=True, primary_key=True, serialize=False)),
                ('count', models.IntegerField()),
                ('Time', models.DateTimeField()),
                ('access_token', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='stream',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('country', models.CharField(max_length=200)),
                ('name', models.CharField(max_length=200)),
                ('bounding_box', models.CharField(max_length=1024)),
                ('capital', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='tokens',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('consumer_key', models.CharField(max_length=200)),
                ('consumer_secret', models.CharField(max_length=200)),
                ('access_token', models.CharField(max_length=200)),
                ('access_key', models.CharField(max_length=200)),
                ('running', models.BooleanField()),
                ('last_error', models.CharField(max_length=1024)),
            ],
        ),
        migrations.CreateModel(
            name='tweets',
            fields=[
                ('_id', djongo.models.fields.ObjectIdField(auto_created=True, primary_key=True, serialize=False)),
                ('country', models.CharField(max_length=10)),
                ('text', models.TextField(null=True)),
                ('cleaned_text', djongo.models.fields.ListField(default=[])),
                ('hashtags', djongo.models.fields.ListField(default=[])),
                ('mentions_screen_name', djongo.models.fields.ListField(default=[])),
                ('screen_name', models.TextField(null=True)),
                ('sentiment_analysis', models.TextField(null=True)),
                ('retweet_screen_name', models.TextField(null=True)),
                ('status_id', models.TextField(null=True)),
                ('created_at', models.DateTimeField(db_index=True)),
                ('timestamp_ms', models.BigIntegerField()),
                ('quoted_screen_name', models.TextField(null=True)),
                ('type', models.TextField(null=True)),
                ('coordinates', djongo.models.fields.ListField()),
                ('is_retweet', models.BooleanField()),
                ('is_quote', models.BooleanField()),
                ('retweet_count', models.IntegerField()),
                ('topics', djongo.models.fields.ListField(default=[])),
                ('profile_image_url_https', models.TextField(null=True)),
            ],
        ),
        migrations.AddIndex(
            model_name='stream',
            index=models.Index(fields=['country'], name='project_str_country_c981bf_idx'),
        ),
    ]
