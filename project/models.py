# This file defines the project and tokens model. They contains fields that will be stored in the Db
from django.db import models
from datetime import date
from djongo import models as s


# Each project model contains a unique id which is a self increamenting value field. 
# Create your models here.
class stream(models.Model):
    country=models.CharField(max_length=200) 
    name=models.CharField(max_length=200)
    bounding_box=models.CharField(max_length=1024)
    capital=models.CharField(max_length=255)	
    running =models.BooleanField()
    class Meta:
       indexes = [
           models.Index(fields=['country'])
		   ]
		   
# Each token has a running field which indicates how many projects are using this token.
class tokens(models.Model):
    consumer_key=models.CharField(max_length=200)
    consumer_secret=models.CharField(max_length=200)
    access_token=models.CharField(max_length=200)
    access_key=models.CharField(max_length=200)
    running=models.BooleanField()
    last_error=models.CharField(max_length=1024)
	
class hourlycount(models.Model):
	_id = s.ObjectIdField(primary_key=True)
	count=models.IntegerField()
	Time=models.DateTimeField()
	access_token=models.CharField(max_length=200)
	
class lastcount(models.Model):
	_id = s.ObjectIdField(primary_key=True)
	count=models.IntegerField()
	Time=models.DateTimeField()
	access_token=models.CharField(max_length=200)    

# The model defined here is used by documents.py to associate the document with the defined model

# Create your models here.
class tweets(models.Model): #Collection Name
    country=models.CharField(max_length=10)
    text =  models.TextField(null=True)
    cleaned_text = s.ListField(default=[])
    hashtags =  s.ListField(default=[])
    mentions_screen_name =s.ListField(default=[])
    screen_name =  models.TextField(null=True)
    sentiment_analysis = models.TextField(null=True)
    type = models.TextField(null=True)
    retweet_screen_name = models.TextField(null=True)
    status_id = models.TextField(null=True)
    created_at=models.DateTimeField()
    timestamp_ms=models.BigIntegerField()
    quoted_screen_name=models.TextField(null=True)
    coordinates=s.ListField()
    is_retweet=models.BooleanField()
    is_quote=models.BooleanField()
    retweet_count=models.IntegerField()
    topics=s.ListField(default=[])
    profile_image_url_https=models.TextField(null=True)


