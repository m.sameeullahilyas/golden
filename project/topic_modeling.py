from nltk.corpus import stopwords
import codecs
import os
from .Preprocessor import preprocessor
from sklearn.feature_extraction.text import TfidfVectorizer
from nltk.tokenize import word_tokenize
from .stopwords import getStopWords,getTopics
import re
from itertools import chain,combinations
import pandas as pd
from pymongo import MongoClient
from collections import Counter
import networkx 
from gensim.models import word2vec
from time import time
from sklearn.decomposition import NMF
from pymongo import UpdateMany

def get_topics(country,min):
	list_of_stopWords = getStopWords()
	stopWords = [preprocessor(stopWord) for stopWord in list_of_stopWords ]
	stopWords = list(filter(None, stopWords))
	stopWords_re = re.compile(r'\b(?:%s)\b' % '|'.join(stopWords))
	list_of_topics = getTopics()
	topics = [preprocessor(topic) for topic in list_of_topics ]
	topics = list(filter(None, topics))
	__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
	INPUT_DIR = os.path.join(__location__, 'pre_topical_keywords.txt')    
	write_topics = open(INPUT_DIR, 'w', encoding = 'utf-8')
	for topic in topics:
		write_topics.write(str(topic) + "\n")
	client = MongoClient('mongodb://localhost:27017/')
	db = client.project
	coll = db.project_tweets
	operations=[]
	pipeline = [{ "$match" : { "country" : country ,"timestamp_ms":{"$gte":int(min)} } }]
	mydoc=coll.aggregate(pipeline)
	data = []
	hashtags = []
	combined_hashtags = []
	tweet_info = dict()
	for t in mydoc:
		clean_tweet=t["cleaned_text"]
		clean_tweet+=" "+" ".join(t["hashtags"])
		clean_tweet=clean_tweet.split(" ")
		clean_tweet=[x for x in clean_tweet if not (x=='') ]
		clean_tweet=list(dict.fromkeys(clean_tweet))
		clean_tweet=" ".join(clean_tweet)
        #Preprocess tweet
		if (len(clean_tweet) > 0):
				data.append(clean_tweet)
				tweet_info[t["status_id"]] = clean_tweet
				if(len(t["hashtags"])>0): # if threre is hashtag in tweet (original tweet)
					hashtags.extend(t["hashtags"])
					unique_hasht = list(dict.fromkeys(t["hashtags"]))
					if(len(unique_hasht)>1):
						combined_hashtags.append(unique_hasht)
	if(len(data) >0):
        #Remove Duplicates                                     
		noDupData = list(dict.fromkeys(data)) 

        # corpus = noDupData OR data
		corpus = noDupData
        
        #Remove Duplicates      
		DupHashtags = hashtags
		hashtags = list(dict.fromkeys(hashtags))
		topics = []
		with codecs.open(INPUT_DIR, "r", encoding='utf-8') as inputs:
				for topic in inputs:
					topics.append( " ".join(topic.split()))
		topics_re = re.compile(r'\b(?:%s)\b' % '|'.join(topics))
		
		#Hatshtag_Tweets_Topics
		#get hashtags (Hashtags are without Duplicates)
		hashtags_re = re.compile(r'\b(?:%s)\b' % '|'.join(hashtags))

		#get Tweets
		data_re = re.compile(r'\b(?:%s)\b' % '|'.join(corpus))
		# Loop over all hashtags, get Tweets  where that hashtag occur, get Topic, Save in Json file
		topic=[]
		result = {}
		Hash_Tweets_Dict = dict()
		for hashtag in hashtags:
			list_tweet =[]
			hashtag_comp = re.compile(r'\b'+hashtag+r'\b')
			for tweet in corpus:
				if(hashtag_comp.search(tweet)):
					list_tweet.append(tweet)
            #list_tweet = list(dict.fromkeys(list_tweet)) # remove duplicate tweets
			Hash_Tweets_Dict[hashtag] = list_tweet
        # loop through each Value in the Dictionary (tweets per hashtags), and get their topics or Null
		topic=[]
		result = {}
		for Key_hash in Hash_Tweets_Dict.keys(): # for each hashtag
			for Value_tweet in Hash_Tweets_Dict[Key_hash]:
				topic = topics_re.findall(Value_tweet.replace("_", " "))
				if(len(topic) > 0):
					result[Key_hash]=({'Hashtag': Key_hash,'Tweets': str(Hash_Tweets_Dict[Key_hash]),'Topics': topic})
				else:
					result[Key_hash]=({'Hashtag': Key_hash,'Tweets': str(Hash_Tweets_Dict[Key_hash]),'Topics': "NONE"})

        #(1)

		unique_combined_hashtags = []
		lineList = []
		unique_combined_hashtags = list(set(chain(*combined_hashtags)))
        #(2)
		hashtags_list = []
		topics_list = []
		for attribute, value in result.items():
			if value["Topics"] != "NONE":
				hashtags_list.append(value['Hashtag'])
				topics_list.extend(value['Topics'])

		if (len(hashtags_list) >0 ):
            #Step #4:
                #Build co-occurrence matrix A of hashtags such that a hashtag co-occurred with hashtag
                #if they both happened to be posted under a same tweet.
            # Get all combinations of pairs
			all_pairs = []
			for c in combined_hashtags:
				pairs = list(combinations(c, 2))
				for p in pairs:
					all_pairs.append(p)

            #all_pairs = list(itertools.combinations(hashtags_list, 2))
			Sorted_all_pairs = [ tuple(sorted(i)) for i in all_pairs ]
			Sorted_Unique_pairs =[]
			for x in Sorted_all_pairs:
				if len(set(x).difference(set(hashtags_list))) == 0:
					Sorted_Unique_pairs.append(x)
			Counter_all_pairs =  Counter(Sorted_Unique_pairs)
			occ_list = []
			for k,v in Counter_all_pairs.items():
				occ_list.append([k[0],k[1],v])
			df_occ = pd.DataFrame(occ_list, columns=['Hashtag1','Hashtag2','occurrences']) 
            # Select co-occurred hashtags based on suitable threshold 
			threshold = 0
			df_topics = df_occ.loc[df_occ['occurrences'] > threshold]
            #Step #6:
                #Based on alpha calcuation, merge hashtags into topics
                #Topics = {topic1, ... topicM} where our defnition by topics as set of
                #related hashtags that both co-occurred with one another and the
                #co-occurrence value exceeding the threshold
			hashtags_co = []
			for row in df_topics.itertuples(index=True):
				topic = (getattr(row, "Hashtag1"),getattr(row, "Hashtag2"))
				hashtags_co.append(topic)        


			topics = []
			g = networkx.Graph(hashtags_co)
			for subgraph in networkx.connected_component_subgraphs(g):
				topics.append(list(subgraph.nodes()))   

                
            # ------------------------ S: Build Word2Vec Model ------------------------------
            #set model's parameters
			feature_size = 100    # Word vector dimensionality  
			window_context = 15   # Context window size  
			min_word_count = 5   # Minimum word count                        
			sample = 1e-3         # Downsample setting for frequent words
			sg = 1                #skip-gram = 1 , CBOW = 0

            # we need to pass splitted sentences to the model
			tokenized_sentences = [sentence.split() for sentence in corpus]

			try:
				w2v_model = word2vec.Word2Vec(tokenized_sentences,size=feature_size, 
                                          window=window_context, min_count = min_word_count,
                                          sample=sample, workers=2, sg=sg)

                #model.save(mongoDir+'Dict_model_SG.mdl')    
                # ------------------------ E: Build Word2Vec Model ------------------------------    
                # ------------------------ S: Extend topics using Word2Vec ------------------------------    
                # Extend the topic by getting the top 5 most similar words using the Word2Vec model for each topic's hashtags     
                    
				topics_extend = []
				for topic in topics:
					topic_extend = []
					for keyword in topic:  
						topic_extend.append(keyword)
						if keyword in w2v_model.wv.vocab:
							most_similar = w2v_model.wv.most_similar(keyword, topn=5)
							for term, score in most_similar:
								topic_extend.append(term)
                    
					topic_extend = list(dict.fromkeys(topic_extend))    
					if(len(topic_extend)>0):
						topics_extend.append(topic_extend)

                # ------------------------ E: Extend topics using Word2Vec ------------------------------        

                # ------------------------ S: Merge Topics ------------------------------    
				topics_extend_set = []

				def to_graph(topics_extend):
					G = networkx.Graph()
					for part in topics_extend:
                        # each sublist is a bunch of nodes
						G.add_nodes_from(part)
                        # it also imlies a number of edges:
						G.add_edges_from(to_edges(part))
					return G

				def to_edges(topics_extend):
					''' 
                        treat `topics_extend` as a Graph and returns it's edges 
                        to_edges(['a','b','c','d']) -> [(a,b), (b,c),(c,d)]
					'''
					it = iter(topics_extend)
					last = next(it)

					for current in it:
						yield last, current
						last = current    

				G = to_graph(topics_extend)
                #print (connected_components(G) )

				for subgraph in networkx.connected_component_subgraphs(G):
					topics_extend_set.append(list(subgraph.nodes()))
			except:
				topics_extend_set = []
				topics_extend_set = topics
                
            # ------------------------ E: Merge Topics ------------------------------    
            # ------------------------ S: Save (Co-occurance hashtags) Date/Topics/Tweets ---------------------------
			for topic in topics_extend_set:
                #topic_withHash = ["#" + key for key in topic]
				topics_re = re.compile(r'\b(?:%s)\b' % '|'.join(topic))
				topics_tweetIDs = []
				for key_tweetID,value_tweetText in tweet_info.items():
					if len(topics_re.findall(value_tweetText)) > 0:
						topics_tweetIDs.append(key_tweetID)    
				records = (coll.find({"status_id":{"$in":topics_tweetIDs}}, { "status_id": 1, "topics": 1, "_id": 0 }))
				for rec in records: 
					rec_tweet_id = rec["status_id"] 
					rec_topic_ids = []
					if "topics" in rec:
						rec_topic_ids.extend(rec["topics"])
						rec_topic_ids.extend(topic)
					else:	
						rec_topic_ids.extend(topic)
					rec_topic_ids=list(dict.fromkeys(rec_topic_ids))
					operations.append(UpdateMany({"status_id":rec_tweet_id},{ "$set": { "topics": rec_topic_ids}}))

            # ------------------------ E: Save (Co-occurance hashtags) Date/Topics/Tweets ----------------------------   
		else:
			print(" No hashtag has topic")
        
        # ------------------------ S: NMF  ------------------------------    			
            #---    Build  NMF model      ---#
		NUM_TOPICS = 5
		NUM_FEATURES = 20000
		NUM_TOP_WORDS = 5
                    
            # -- Feature Extraction --
            # Use tf-idf features for NMF.
		t0 = time()
		tfidf_vectorizer = TfidfVectorizer(max_df=95, min_df=1,
                                               encoding='utf-8'
                                               #,max_features=NUM_FEATURES
                                               , ngram_range=(1,2)
                                               )
		tfidf = tfidf_vectorizer.fit_transform(corpus)


            # -- Build the Models  -- 
            # Build a Non-Negative Matrix Factorization Model (NMF)
		t0 = time()
            
		if NUM_TOPICS >len(corpus):
			NUM_TOPICS = len(corpus)
            
		nmf_model = NMF(n_components=NUM_TOPICS, init='nndsvd')
		nmf_Z = nmf_model.fit_transform(tfidf)
           # ------------------------ E: NMF  ------------------------------    

            # ------------------------ S: Save (NMF) Date/Topics/Tweets ------------------------------    
		for idx, topic in enumerate(nmf_model.components_):
			nmf_topic = (",".join([tfidf_vectorizer.get_feature_names()[i]
                           for i in topic.argsort()[:-NUM_TOP_WORDS - 1:-1]]))
			topics_re = re.compile(r'\b(?:%s)\b' % '|'.join(nmf_topic.split(",")))
			topics_tweetIDs = []
			for key_tweetID,value_tweetText in tweet_info.items():
				if len(topics_re.findall(value_tweetText)) > 0:
					topics_tweetIDs.append(key_tweetID)    
			
			records = (coll.find({"status_id":{"$in":topics_tweetIDs}}, { "status_id": 1, "topics": 1, "_id": 0 }))
			for rec in records: 
				rec_tweet_id = rec["status_id"] 
				rec_topic_ids=[]
				try:
					nmf_topic=nmf_topic.split(",")
				except:
					pass
				if "topics" in rec:
					rec_topic_ids.extend(rec["topics"])
					rec_topic_ids.extend(nmf_topic)
				else:
					rec_topic_ids.extend(nmf_topic)
				rec_topic_ids=list(dict.fromkeys(rec_topic_ids))	
				operations.append(UpdateMany({"status_id":rec_tweet_id},{ "$set": { "topics": rec_topic_ids}}))
		coll.bulk_write(operations)
	   # ------------------------ E: Save (NMF) Date/Topics/Tweets ------------------------------       
	else:
		print("No Data Found")
	return 		


