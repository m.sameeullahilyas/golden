from nltk.corpus import stopwords
import codecs
import os
    
		# prepare the stop words list
def getStopWords():
        arStopwords = stopwords.words('arabic')
        enStopWords = stopwords.words('english')
        newStopWords = []
        __location__ = os.path.realpath(
        os.path.join(os.getcwd(), os.path.dirname(__file__)))
        INPUT_DIR = os.path.join(__location__, 'arabic_stopwords.txt')
        file = codecs.open(INPUT_DIR, "r", encoding='utf-8')
        for line in file:
            line = " ".join(line.split())
            newStopWords.append(line)
        arStopwords.extend(enStopWords)
        arStopwords.extend(newStopWords)
        return set(arStopwords)

def getTopics():
    __location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
    INPUT_DIR = os.path.join(__location__, 'topical_keywords.txt')    
    topicalWords_file = codecs.open(INPUT_DIR, "r", encoding='utf-8')
    list_of_topicalWords =[]
    for word in topicalWords_file:
        list_of_topicalWords.append(" ".join(word.split()))

    __location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
    INPUT_DIR = os.path.join(__location__, 'Persian-Topical-keywords.txt')    
    topicalWords_file = codecs.open(INPUT_DIR, "r", encoding='utf-8')
    for word in topicalWords_file:
        list_of_topicalWords.append(" ".join(word.split()))
    return list_of_topicalWords

def getPersianStopWords():
        enStopWords = stopwords.words('english')
        newStopWords = []
        __location__ = os.path.realpath(
        os.path.join(os.getcwd(), os.path.dirname(__file__)))
        INPUT_DIR = os.path.join(__location__, 'short.txt')
        file = codecs.open(INPUT_DIR, "r", encoding='utf-8')
        for line in file:
            line = " ".join(line.split())
            newStopWords.append(line)
        newStopWords.extend(enStopWords)
        return set(newStopWords)

def getrestricted():
    __location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
    INPUT_DIR = os.path.join(__location__, 'restricted-words.txt') 
    restrictedWords_file = codecs.open(INPUT_DIR, "r", encoding='utf-8')
    list_of_restricted =[]
    for word in restrictedWords_file:
        list_of_restricted.append(" ".join(word.split()))

    return list_of_restricted

