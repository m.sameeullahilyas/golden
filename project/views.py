# This file contains functions that are used for starting, stopping, deleting and other functions that are related to projects
from django.shortcuts import render,redirect
from login.models import user
from .models import stream,tokens
from .Stream import start_project
from threading import Thread,Event

# A global list that will contain the corresponding events of the projects to see if they are atopped by the user or not.
list={}
# Create your views here.
# Function for generating the home page
def home(request):
# Check if user is logged in or not
    if ('user' not in request.session) or ('pass' not in request.session):
        return redirect('/login')
    
    try:
        user_id=user.objects.get(email=request.session['user'],password=request.session['pass'])
        id=user_id.email

    except user.DoesNotExist:
        request.session['error_msg']= "Not Logged In"
        return redirect('/login')
    country_list=stream.objects.filter() 
    countries=[]
    for country in country_list:
          countries.append(country.country)
    context={
	"countries":countries
	}
    return render(request,'index.html',context)

# Function to edit the account settings of the user. If new email is entered then user will have to reverify the email. A new token is sent to the user for
# verification
def account_edit(request):
    user_edit=user.objects.get(email=request.session['user'])
    if request.method=="POST":
        user_edit.first_name=request.POST.get("First Name")
        user_edit.last_name=request.POST.get("Last Name")
        user_edit.phone=request.POST.get("Phone")
        if user_edit.password!=request.POST.get("Password"):
           password=(request.POST.get('Password'))
           user_edit.password=password
        if user_edit.email!=request.POST.get("email"):
           project.objects.filter(user=user_edit.email).update(user=request.POST.get("email"))
           user_edit.email=request.POST.get("email")
           user_edit.is_verified=False
          
        try:
           user_edit.save()
           if not user_edit.is_verified:
               mail_subject = 'Activate your account.'
               current_site = get_current_site(request)
               message = render_to_string('acc_active_email.html', {
                'First_Name': user_edit.first_name,
                'Last_Name': user_edit.last_name,
                'Phone#': user_edit.phone,
                'domain': current_site.domain,
                'uid':urlsafe_base64_encode(force_bytes(user_edit._id)),
                'token':account_activation_token.make_token(user_edit),
               })
			
               toemail = user_edit.email
               email = EmailMessage(
                        mail_subject, message, to=[toemail]
                )
               email.send()
               context={"success_msg":"Account details are changed. Since Email is changed so check your mail for verification otherwise you wont be able to login"}
				
           else:
               context={"success_msg":"Account details are successfully changed"}
        except Exception as e:
               context={"error_msg":e}
	
        if "success_msg" in context:
            request.session["success_msg"]=context["success_msg"]
            request.session["user"]=user_edit.email
            request.session["pass"]=user_edit.password
            return redirect('/')
        elif "error_msg" in context:
            context["user"]=user_edit
    else:
          context={"user":user_edit}
    return render(request,'account_edit.html',context)
# Function to start a new thread when a new project is being created. This function is a decorator. Each new thread that will be responsible for
# handling the stream of the project will have a unique name equal to project<id> where id is the unique id of the project. The thread will be a daemon
# thread. This unique name will be used for identifying whether a project is running or not and also for stopping the project
def start_new_thread(function):
    def decorator(*args, **kwargs):
        nameproject=str(args[0])
        t = Thread(name=nameproject,target = function, args=args, kwargs=kwargs)
        t.daemon = True
        t.start()
    return decorator
# Starting the new thread with decorator mentioned. Supplies the start_project function with required parameters
@start_new_thread
def start_new_project(country,coordinates,capital):

  coordinates=coordinates.split(",")
  coordinates=[float(x) for x in coordinates]
  while not list[country].is_set():
      start_project(country,coordinates,capital)


# Function to start the project. This will create a new event and save it to list with key being project<id> i.e. the event is being saved with a key that is
# equal to the name of the thread that will be created. This will be used to stop the project. Having same key as name of thread, we can identify which 
# event is of which project and can set the event which will result in stopping of the project.

