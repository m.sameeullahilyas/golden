The project contains three applications

1) Login:
        It handles the login, registration, forgot and other such processes of users
		
2) project:
        It handles the creation, starting, stopping and other related features
		
3) visualize:
        Handles the graphing of the data and searching of projects

The network analysis tool is saved in the public repository:
									https://gitlab.com/m.sameeullahilyas/tweet
									
More details can be found in the respective directories.

Requirements.txt defines the requirements for running the project
Also elasticsearch server 6.4.0 is used
for R language shiny server is deployed on port 3838
