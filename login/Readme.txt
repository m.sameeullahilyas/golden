This application is responsible for handling the login, registeration, forgot and reset password functions for the users.
The main files for this application are:

1) View.py
2) Models.py
3) Admin.py
4) tokens.py

1) View.py:
          Contains functions that will handle the login, registeration, forgot and reset functions for the user. Also contains other functions like
generating token that will identify if the token given in the link is derived from a valid user or not.

2) Models.py:
            This file is used to define the structure of the user model. It defines the fields that will be stored inside the Db. Every user will have
the fields defined in this model.

3) Admin.py:
           This file is used to setup the admin panel. It contains settings for the user option displayed under login in the admin panel			

4) tokens.py:
            The tokengenerator() function defined in this file is used by views.py to derive a hashed token from the objectid of the user and then 
the same token will be used to derive the objectid to see if the link that is opened contains a valid user or not

The template files for the functions are :

1) login.html
2) Register.html
3) acc_active_email.html (Email to send when new user is created)
4) acc_forgot_password.html (Email to send when user asks for resetting password)
5) forgot.html 
6) reset.html

