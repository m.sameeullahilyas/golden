"""Twitter URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,re_path
from login import views
from project import views as v
from visualize import views as v1

# maps the urls to the functions
urlpatterns = [
    path('admin/', admin.site.urls),
    re_path(r'^account_edit/', v.account_edit),
    path('forgot/', views.forgot),
    path('login', views.login),
    path('logout', views.logout),
    path('', v.home),
	    re_path(r'^search/(?P<country>[A-Z]+$)', v1.search),
    re_path(r'^(?P<country>[A-Z]+$)', v1.visualize),
    re_path(r'^(?P<country>[A-Z]+)/timeline/(?P<min>\d{0,15})/(?P<max>\d{10,18})/(?P<top>[A-Za-z]+)/(?P<word>.*)/$', v1.timeline),
    re_path(r'^(?P<country>[A-Z]+)/timeline_data/(?P<min>\d{0,15})/(?P<max>\d{10,18})/(?P<top>[A-Za-z]+)/(?P<word>.*)/$', v1.timeline_data),
    re_path(r'^(?P<country>[A-Z]+)/network/(?P<min>\d{0,15})/(?P<max>\d{10,18})/(?P<top>[A-Za-z]+)/(?P<word>.*)/(?P<skip>[0-9]+)/$', v1.network),
    re_path(r'^(?P<country>[A-Z]+)/map/(?P<min>\d{0,15})/(?P<max>\d{10,18})/(?P<top>[A-Za-z]+)/(?P<word>.*)/$', v1.map),
    re_path(r'^(?P<country>[A-Z]+)/map_data/(?P<min>\d{0,15})/(?P<max>\d{10,18})/(?P<top>[A-Za-z]+)/(?P<word>.*)/(?P<skip>[0-9]+)/$', v1.map_data),
    re_path(r'^(?P<country>[A-Z]+)/hashtags/(?P<min>\d{0,15})/(?P<max>\d{10,18})/(?P<top>[A-Za-z]+)/(?P<word>.*)/$', v1.hashtags),
	    re_path(r'^(?P<country>[A-Z]+)/viral/(?P<min>\d{0,15})/(?P<max>\d{10,18})/(?P<top>[A-Za-z]+)/(?P<word>.*)/(?P<skip>[0-9]+)/$', v1.get_viral),
    re_path(r'^(?P<country>[A-Z]+)/virality/(?P<min>\d{0,15})/(?P<max>\d{10,18})/(?P<top>[A-Za-z]+)/(?P<word>.*)/$', v1.get_virality),
    re_path(r'^(?P<country>[A-Z]+)/screen_name/(?P<min>\d{0,15})/(?P<max>\d{10,18})/(?P<top>[A-Za-z]+)/(?P<word>.*)/$', v1.screen_name),
    re_path(r'^(?P<country>[A-Z]+)/mentions_screen_name/(?P<min>\d{0,15})/(?P<max>\d{10,18})/(?P<top>[A-Za-z]+)/(?P<word>.*)/$', v1.mentions_screen_name),
    re_path(r'^(?P<country>[A-Z]+)/sentiments/(?P<min>\d{0,15})/(?P<max>\d{10,18})/(?P<top>[A-Za-z]+)/(?P<word>.*)/$', v1.sentiments),
	    re_path(r'^(?P<country>[A-Z]+)/twitter/(?P<min>\d{0,15})/(?P<max>\d{10,18})/(?P<sentiment>[a-zA-Z]+)/(?P<type>[a-zA-Z]+)/(?P<top>[A-Za-z]+)/(?P<word>.*)/(?P<skip>[0-9]+)/$', v1.text1),
    re_path(r'^(?P<country>[A-Z]+)/twitter/(?P<min>\d{0,15})/(?P<max>\d{10,18})/(?P<sentiment>[a-zA-Z]+)/(?P<type>[a-zA-Z]+)/(?P<top>[A-Za-z]+)/(?P<word>.*)/$', v1.text),
	    re_path(r'^(?P<country>[A-Z]+)/word/(?P<min>\d{0,15})/(?P<max>\d{10,18})/(?P<sentiment>[a-zA-Z]+)/(?P<type>[a-zA-Z]+)/(?P<top>[A-Za-z]+)/(?P<word>.*)/(?P<assoc>.*)/$', v1.wordassoc),
    re_path(r'^(?P<country>[A-Z]+)/word/(?P<min>\d{0,15})/(?P<max>\d{10,18})/(?P<sentiment>[a-zA-Z]+)/(?P<type>[a-zA-Z]+)/(?P<top>[A-Za-z]+)/(?P<word>.*)/$', v1.wordcloud),
    path('register', views.register),
    re_path(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
views.activate, name='activate'),
    re_path(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
views.reset, name='reset'),
]
