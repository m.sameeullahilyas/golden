"""
WSGI config for Twitter project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.1/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application
from pymongo import MongoClient
client = MongoClient('mongodb://localhost:27017/')
db = client.project
db.project_tokens.update_many({},{"$set":{"running":False}})

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'Twitter.settings')

application = get_wsgi_application()
