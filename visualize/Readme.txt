This directory contains functions that are related to visualizing the data. The main files are:

1) views.py:
           This file contains functions that are responsible for rendering charts. Id is the parameter that references a unique project. Top is the
word that is used to distinguish between different types of content i.e. hashtags, mentions, screen and all. if top is equal to top then all data is collected
without applying any condition. If top is equal to hashtags then only those tweets data will be collected which have a hashtag equal to the parameter "word"
The word will contain a hashtags. This word is mostly passed on clicking on the graph. The type parameter used in wordcloud and text is used to distinguish
between different types of tweets i.e. links, pictures, videos and others.

2) documents.py:
            Used to define the document of the index of elasticsearch.
			
3) models.py:
            Used to define the model of tweets being collected and which fields will be indexed in the elasticsearch

The templates used are:


1) map.html
2) hashtags.html
3) mentions_screen_name.html
4) visualize.html
5) screen.html
6) word-cloud.html
7) text.html
8) sentiments.html
9) search.html