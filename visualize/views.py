# The functions defined here are mostly used inside the visualize page
from compression_middleware.decorators import compress_page
from django.shortcuts import render,redirect
from login.models import user
from project.models import stream,tweets
from visualize.topic import get_topics
# Create your views here.
from .documents import TweetsDocument
from collections import Counter
import re
import numpy as np
from datetime import datetime,timedelta
import json
from django.shortcuts import HttpResponse
import time

# Function to render the visualize page.
@compress_page
def visualize(request,country):

    if ('user' not in request.session) or ('pass' not in request.session):
        return redirect('/login')

    try:
        user_id=user.objects.get(email=request.session['user'],password=request.session['pass'])
        idd=user_id.email
    except user.DoesNotExist:
        request.session['error_msg']= "Not Logged In"
        return redirect('/login')
    
    name=stream.objects.get(country=country)
    body={ "size":0,"aggs" : { "minimum" : { "filter" : { "term": { "country": country } }, "aggs" : { "min_timestamp" : { "min" : { "field" : "timestamp_ms" } },"max_timestamp" : { "max" : { "field" : "timestamp_ms" } } } } } }
    s=TweetsDocument.search().from_dict(body)
    t=s.execute()
    count=t.aggregations.minimum.doc_count
    min=int(t.aggregations.minimum.min_timestamp.value)
    max=int(t.aggregations.minimum.max_timestamp.value)
    context={"country":country,"length":count,"min":min,"name":name.name,"max":max}
    return render(request,'visualize.html',context)

# Function to display the timeline. The timestamp_ms stored in the Db is aggregated and sorted in a descending order which will be used bytearray
# highstocks intraday chart to display the timeline. country is the unique parameter referencing a unique project. min max corresponds to the time 
# during which data is to be collected. Top refers to the type of content that is to be collected meaning mentions hashtags screen or all
# if mentions is selected then only those values are selected which have the mentions equal to the word parameter. Same applies for screen and hashtags	
@compress_page
def timeline_data(request,country,min,max,top,word):
    if top=="top":
        body = {"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }} ] } }, "aggs" : { "timeline" : { "auto_date_histogram" : { "field" : "timestamp_ms", "buckets" : 333,"minimum_interval":"minute"} } } }
    elif top=="mentions":
        body={"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"mentions_screen_name.raw":word}} ] } }, "aggs" : { "timeline" : { "auto_date_histogram" : { "field" : "timestamp_ms", "buckets" : 333} } } }
    elif top=="hashtags":
        body={"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"hashtags.raw":word}} ] } }, "aggs" : { "timeline" : { "auto_date_histogram" : { "field" : "timestamp_ms", "buckets" : 333} } } }
    elif top=="screen":
        body={"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"screen_name.raw":word}} ] } }, "aggs" : { "timeline" : { "auto_date_histogram" : { "field" : "timestamp_ms", "buckets" : 333} } } }
    elif top=="topic":
        body={"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"topics":word}} ] } }, "aggs" : { "timeline" : { "auto_date_histogram" : { "field" : "timestamp_ms", "buckets" : 333} } }  }
    time.sleep(2)
    s=TweetsDocument.search().from_dict(body)
    try:
        t=s.execute()
    except:
       if top=="top":
              body = {"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }} ] } }, "aggs" : { "timeline" : { "auto_date_histogram" : { "field" : "timestamp_ms", "buckets" : 333,"minimum_interval":"hour"} } } }
              s=TweetsDocument.search().from_dict(body)
              t=s.execute()
    json_data=[]
    for item in t.aggregations.timeline:
            json_data.append([item.key,item.doc_count])
    
    response=HttpResponse(json.dumps(json_data),content_type='application/json')
    return response

@compress_page
def timeline(request,country,min,max,top,word):
    return render(request,'timeline.html',{"country":country})
# Function to display the top10 hashtags.
@compress_page
def hashtags(request,country,min,max,top,word):
    if top=="top":
        body={"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }} ] } }, "aggs" : { "hashtags" : { "terms" : { "field" : "hashtags.raw", "size" : 10 } }  } }  
    elif top=="mentions":
        body={"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"mentions_screen_name.raw":word }} ] } }, "aggs" : { "hashtags" : { "terms" : { "field" : "hashtags.raw", "size" : 10 } }  } }
    elif top=="hashtags":
        body={"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"hashtags.raw":word }} ] } }, "aggs" : { "hashtags" : { "terms" : { "field" : "hashtags.raw", "size" : 10 } }  } }
    elif top=="screen":
        body= {"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"screen_name.raw":word }} ] } }, "aggs" : { "hashtags" : { "terms" : { "field" : "hashtags.raw", "size" : 10 } }  } }  
    elif top=="topic":
        body= {"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"topics":word }} ] } }, "aggs" : { "hashtags" : { "terms" : { "field" : "hashtags.raw", "size" : 10 } }  } }

    s=TweetsDocument.search().from_dict(body)
    t=s.execute()
    hashtags_data=[]
    for item in t.aggregations.hashtags:
        hashtags_data.append([item.key,item.doc_count]) 
    context={"hashtags":hashtags_data,"country":country}
    return render(request,'hashtags.html',context)
# Function to display the top10 users
@compress_page
def screen_name(request,country,min,max,top,word):
    if top=="top":
        body={"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }} ] } }, "aggs" : { "screen" : { "terms" : { "field" : "screen_name.raw", "size" : 10 } }  } } 
    elif top=="hashtags":
        body = {"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"hashtags.raw":word }} ] } }, "aggs" : { "screen" : { "terms" : { "field" : "screen_name.raw", "size" : 10 } }  } } 
    elif top=="mentions":
        body={"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"mentions_screen_name.raw":word }} ] } }, "aggs" : { "screen" : { "terms" : { "field" : "screen_name.raw", "size" : 10 } }  } } 
    elif top=="topic":
        body={"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"topics":word }} ] } }, "aggs" : { "screen" : { "terms" : { "field" : "screen_name.raw", "size" : 10 } }  } } 
    else:
        body={"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"screen_name.raw":word }} ] } }, "aggs" : { "screen" : { "terms" : { "field" : "screen_name.raw", "size" : 10 } }  } }
    s=TweetsDocument.search().from_dict(body)
    t=s.execute()
    screen_name_data=[]
    for item in t.aggregations.screen:
            screen_name_data.append([item.key,item.doc_count])
    context={"screen_name":screen_name_data,"country":country}
    return render(request,'screen_name.html',context)
# Function to display the top10 mentions
@compress_page
def mentions_screen_name(request,country,min,max,top,word):
    if top=="top":
        body={"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }} ] } }, "aggs" : { "mentions" : { "terms" : { "field" : "mentions_screen_name.raw", "size" : 10 } }  } }  
    elif top=="hashtags":
        body={"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"hashtags.raw":word }} ] } }, "aggs" : { "mentions" : { "terms" : { "field" : "mentions_screen_name.raw", "size" : 10 } }  } } 
    elif top=="screen":
        body={"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"screen_name.raw":word }} ] } }, "aggs" : { "mentions" : { "terms" : { "field" : "mentions_screen_name.raw", "size" : 10 } }  } }   
    elif top=="topic":
        body={"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"topics":word }} ] } }, "aggs" : { "mentions" : { "terms" : { "field" : "mentions_screen_name.raw", "size" : 10 } }  } } 
    else:
        body={"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"mentions_screen_name.raw":word }} ] } }, "aggs" : { "mentions" : { "terms" : { "field" : "mentions_screen_name.raw", "size" : 10 } }  } } 
    s=TweetsDocument.search().from_dict(body)
    t=s.execute()
    mentions_screen_name_data=[]
    for item in t.aggregations.mentions:
            mentions_screen_name_data.append([item.key,item.doc_count]) 
    context={"mentions_screen_name":mentions_screen_name_data,"country":country}
    return render(request,'mentions_screen_name.html',context)
# Function to display the sentiment pie chart
@compress_page
def sentiments(request,country,min,max,top,word):
    if top=="top":
        body={"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }} ] } }, "aggs" : { "sentiment" : { "terms" : { "field" : "sentiment_analysis"} }  } }          
    elif top=="mentions":
        body={"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"mentions_screen_name.raw":word }} ] } }, "aggs" : { "sentiment" : { "terms" : { "field" : "sentiment_analysis"} }  } } 
    elif top=="hashtags":
        body={"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"hashtags.raw":word }} ] } }, "aggs" : { "sentiment" : { "terms" : { "field" : "sentiment_analysis"} }  } } 
    elif top=="topic":
        body={"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"topics":word }} ] } }, "aggs" : { "sentiment" : { "terms" : { "field" : "sentiment_analysis"} }  } } 
    else:
        body={"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"screen_name.raw":word }} ] } }, "aggs" : { "sentiment" : { "terms" : { "field" : "sentiment_analysis"} }  } } 

    s=TweetsDocument.search().from_dict(body)
    t=s.execute()
    Positive=0
    Negative=0
    Neutral= 0
    for item in t.aggregations.sentiment:
        if item.key=="Positive":
              Positive=item.doc_count
        elif item.key=="Negative":
              Negative=item.doc_count
        else:
              Neutral=item.doc_count
    sentiment_data=[Positive,Negative,Neutral]
    length=Positive+Negative+Neutral
    context={"sentiments":sentiment_data,"length":length,"country":country}
    return render(request,'sentiments.html',context)
# Function to display the tweet feed
@compress_page
def text(request,country,min,max,sentiment,type,top,word):
    return render(request,'text.html')

@compress_page
def text1(request,country,min,max,sentiment,type,top,word,skip):
    limit=10
    if top=="top":
       if sentiment=="All":
           if type !="all":
               body= {"_source":["status_id"],"size":limit,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"type":type }} ] } } }
           else:
               body= {"_source":["status_id"],"size":limit,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }}] } }}
       elif type!="all":
         body = {"_source":["status_id"],"size":limit,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"type":type }},{"term":{"sentiment_analysis":sentiment }}] } } }
       else:
         body = {"_source":["status_id"],"size":limit,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"sentiment_analysis":sentiment }}] } } }
    
    elif top=="topic":
       if sentiment=="All":
           if type !="all":
                body={"_source":["status_id"],"size":limit,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"type":type }} ,{"term":{"topics":word }} ] } } }
           else:
                body={"_source":["status_id"],"size":limit,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"topics":word }} ] } } }
       elif type!="all":
         body={"_source":["status_id"],"size":limit,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"type":type }} ,{"term":{"topics":word }} ,{"term":{"sentiment_analysis":sentiment }}] } } }
       else:
         body={"_source":["status_id"],"size":limit,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"topics":word }} ,{"term":{"sentiment_analysis":sentiment }}] } } }
    
    elif top=="hashtags":
       if sentiment=="All":
          if type !="all":
                body={"_source":["status_id"],"size":limit,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"type":type }} ,{"term":{"hashtags.raw":word }} ] } } }
          else:
                body={"_source":["status_id"],"size":limit,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"hashtags.raw":word }} ] } } }
       elif type!="all":
         body={"_source":["status_id"],"size":limit,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"type":type }} ,{"term":{"hashtags.raw":word }} ,{"term":{"sentiment_analysis":sentiment }}] } } }
       else:
         body={"_source":["status_id"],"size":limit,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"hashtags.raw":word }} ,{"term":{"sentiment_analysis":sentiment }}] } } } 
    elif top=="mentions":
       if sentiment=="All":
          if type !="all":
                body={"_source":["status_id"],"size":limit,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"type":type }} ,{"term":{"mentions_screen_name.raw":word }} ] } } }
          else:
                body={"_source":["status_id"],"size":limit,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"mentions_screen_name.raw":word }} ] } } }
       elif type!="all":
         body={"_source":["status_id"],"size":limit,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"type":type }} ,{"term":{"mentions_screen_name.raw":word }} ,{"term":{"sentiment_analysis":sentiment }}] } } }
       else:
         body={"_source":["status_id"],"size":limit,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"mentions_screen_name.raw":word }} ,{"term":{"sentiment_analysis":sentiment }}] } } }
    else :
       if sentiment=="All":
          if type !="all":
                body={"_source":["status_id"],"size":limit,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"type":type }} ,{"term":{"screen_name.raw":word }} ] } } }
          else:
                body={"_source":["status_id"],"size":limit,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"screen_name.raw":word }} ] } } }
       elif type!="all":
         body={"_source":["status_id"],"size":limit,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"type":type }} ,{"term":{"screen_name.raw":word }} ,{"term":{"sentiment_analysis":sentiment }}] } } }
       else:
         body={"_source":["status_id"],"size":limit,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"screen_name.raw":word }} ,{"term":{"sentiment_analysis":sentiment }}] } } }

    s=TweetsDocument.search().from_dict(body).sort("-created_at").params(preserve_order=True)

    text_data=[]
    i=0
    for hit in s.scan():
        i+=1
        if i>int(skip)+10:
                 break
        if i>int(skip):
            text_data.append(hit.status_id)

    response=HttpResponse(json.dumps(text_data),content_type='application/json') 
    return response    

# Function to display the word cloud and association
@compress_page
def wordcloud(request,country,min,max,sentiment,type,top,word):
    if top=="top":
       if sentiment=="All":
           if type !="all":
                  body={"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"type":type }} ] } }, "aggs" : { "wordcloud" : { "terms" : { "field" : "cleaned_text" ,"size":50} }  } }
           else:
                  body={"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }}] } }, "aggs" : { "wordcloud" : { "terms" : { "field" : "cleaned_text" ,"size":50} }  } } 
       elif type!="all":
         body={"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"type":type }},{"term":{"sentiment_analysis":sentiment }} ] } }, "aggs" : { "wordcloud" : { "terms" : { "field" : "cleaned_text" ,"size":50} }  } } 
       else:
         body={"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"sentiment_analysis":sentiment }} ] } }, "aggs" : { "wordcloud" : { "terms" : { "field" : "cleaned_text" ,"size":50} }  } }   
    elif top=="topic":
       if sentiment=="All":
           if type !="all":
                  body = {"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"topics":word }},{"term":{"type":type }} ] } }, "aggs" : { "wordcloud" : { "terms" : { "field" : "cleaned_text" ,"size":50} }  } }  
           else:
                  body = {"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"topics":word }} ] } }, "aggs" : { "wordcloud" : { "terms" : { "field" : "cleaned_text" ,"size":50} }  } }   
       elif type!="all":
         body = {"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"topics":word }},{"term":{"type":type }},{"term":{"sentiment_analysis":sentiment }} ] } }, "aggs" : { "wordcloud" : { "terms" : { "field" : "cleaned_text" ,"size":50} }  } } 
       else:
         body = {"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"topics":word }},{"term":{"sentiment_analysis":sentiment }} ] } }, "aggs" : { "wordcloud" : { "terms" : { "field" : "cleaned_text" ,"size":50} }  } }
    elif top=="hashtags":
       if sentiment=="All":
           if type !="all":
                  body = {"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"hashtags.raw":word }},{"term":{"type":type }} ] } }, "aggs" : { "wordcloud" : { "terms" : { "field" : "cleaned_text" ,"size":50} }  } }  
           else:
                  body = {"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"hashtags.raw":word }} ] } }, "aggs" : { "wordcloud" : { "terms" : { "field" : "cleaned_text" ,"size":50} }  } }   
       elif type!="all":
         body = {"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"hashtags.raw":word }},{"term":{"type":type }},{"term":{"sentiment_analysis":sentiment }} ] } }, "aggs" : { "wordcloud" : { "terms" : { "field" : "cleaned_text" ,"size":50} }  } } 
       else:
         body = {"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"hashtags.raw":word }},{"term":{"sentiment_analysis":sentiment }} ] } }, "aggs" : { "wordcloud" : { "terms" : { "field" : "cleaned_text" ,"size":50} }  } }
    elif top=="mentions":
       if sentiment=="All":
           if type !="all":
                  body = {"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"mentions_screen_name.raw":word }},{"term":{"type":type }} ] } }, "aggs" : { "wordcloud" : { "terms" : { "field" : "cleaned_text" ,"size":50} }  } }  
           else:
                  body = {"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"mentions_screen_name.raw":word }} ] } }, "aggs" : { "wordcloud" : { "terms" : { "field" : "cleaned_text" ,"size":50} }  } }   
       elif type!="all":
         body = {"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"mentions_screen.raw":word }},{"term":{"type":type }},{"term":{"sentiment_analysis":sentiment }} ] } }, "aggs" : { "wordcloud" : { "terms" : { "field" : "cleaned_text" ,"size":50} }  } } 
       else:
         body = {"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"mentions_screen_name.raw":word }},{"term":{"sentiment_analysis":sentiment }} ] } }, "aggs" : { "wordcloud" : { "terms" : { "field" : "cleaned_text" ,"size":50} }  } }        
    else :
       if sentiment=="All":
           if type !="all":
                  body = {"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"screen_name.raw":word }},{"term":{"type":type }} ] } }, "aggs" : { "wordcloud" : { "terms" : { "field" : "cleaned_text" ,"size":50} }  } }  
           else:
                  body = {"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"screen_name.raw":word }} ] } }, "aggs" : { "wordcloud" : { "terms" : { "field" : "cleaned_text" ,"size":50} }  } }   
       elif type!="all":
         body = {"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"screen.raw":word }},{"term":{"type":type }},{"term":{"sentiment_analysis":sentiment }} ] } }, "aggs" : { "wordcloud" : { "terms" : { "field" : "cleaned_text" ,"size":50} }  } } 
       else:
         body = {"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"screen_name.raw":word }},{"term":{"sentiment_analysis":sentiment }} ] } }, "aggs" : { "wordcloud" : { "terms" : { "field" : "cleaned_text" ,"size":50} }  } } 
        
    s=TweetsDocument.search().from_dict(body)
    t=s.execute()
    freq_data=[]
    for item  in t.aggregations.wordcloud:
        freq_data.append([item.key,item.doc_count])
    context={"text":freq_data}
    return render(request,'word-cloud.html',context)

@compress_page
def wordassoc(request,country,min,max,sentiment,type,top,word,assoc):
    if assoc=='1'or assoc=="_":
       context={'text':"1"}
       return render(request,'word-assoc.html',context)
    if top=="top":
       if sentiment=="All":
           if type !="all":
                  body={"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"type":type }},{"term":{"cleaned_text":assoc}} ] } }, "aggs" : { "wordassoc" : { "terms" : { "field" : "cleaned_text" ,"size":50} }  } }
           else:
                  body={"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"cleaned_text":assoc}}] } }, "aggs" : { "wordassoc" : { "terms" : { "field" : "cleaned_text" ,"size":50}}}}
       elif type!="all":
         body={"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"type":type }},{"term":{"sentiment_analysis":sentiment }} ,{"term":{"cleaned_text":assoc}}] } }, "aggs" : { "wordassoc" : { "terms" : { "field" : "cleaned_text" ,"size":50} }  } } 
       else:
         body={"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"sentiment_analysis":sentiment }},{"term":{"cleaned_text":assoc}} ] } }, "aggs" : { "wordassoc" : { "terms" : { "field" : "cleaned_text" ,"size":50} }  } }   
    elif top=="topic":
       if sentiment=="All":
           if type !="all":
                  body = {"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"topics":word }},{"term":{"type":type }},{"term":{"cleaned_text":assoc}} ] } }, "aggs" : { "wordassoc" : { "terms" : { "field" : "cleaned_text" ,"size":50} }  } }  
           else:
                  body = {"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"topics":word }} ,{"term":{"cleaned_text":assoc}}] } }, "aggs" : { "wordassoc" : { "terms" : { "field" : "cleaned_text" ,"size":50} }  } }   
       elif type!="all":
         body = {"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"topics":word }},{"term":{"type":type }},{"term":{"sentiment_analysis":sentiment }},{"term":{"cleaned_text":assoc}} ] } }, "aggs" : { "wordassoc" : { "terms" : { "field" : "cleaned_text" ,"size":50} }  } } 
       else:
         body = {"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"topics":word }},{"term":{"sentiment_analysis":sentiment }},{"term":{"cleaned_text":assoc}} ] } }, "aggs" : { "wordassoc" : { "terms" : { "field" : "cleaned_text" ,"size":50} }  } }
    elif top=="hashtags":
       if sentiment=="All":
           if type !="all":
                  body = {"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"hashtags.raw":word }},{"term":{"type":type }},{"term":{"cleaned_text":assoc}} ] } }, "aggs" : { "wordassoc" : { "terms" : { "field" : "cleaned_text" ,"size":50} }  } }  
           else:
                  body = {"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"hashtags.raw":word }},{"term":{"cleaned_text":assoc}} ] } }, "aggs" : { "wordassoc" : { "terms" : { "field" : "cleaned_text" ,"size":50} }  } }   
       elif type!="all":
         body = {"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"hashtags.raw":word }},{"term":{"type":type }},{"term":{"sentiment_analysis":sentiment }} ,{"term":{"cleaned_text":assoc}}] } }, "aggs" : { "wordassoc" : { "terms" : { "field" : "cleaned_text" ,"size":50} }  } } 
       else:
         body = {"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"hashtags.raw":word }},{"term":{"sentiment_analysis":sentiment }},{"term":{"cleaned_text":assoc}} ] } }, "aggs" : { "wordassoc" : { "terms" : { "field" : "cleaned_text" ,"size":50} }  } }
    elif top=="mentions":
       if sentiment=="All":
           if type !="all":
                  body = {"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"mentions_screen_name.raw":word }},{"term":{"type":type }} ,{"term":{"cleaned_text":assoc}}] } }, "aggs" : { "wordassoc" : { "terms" : { "field" : "cleaned_text" ,"size":50} }  } }  
           else:
                  body = {"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"mentions_screen_name.raw":word }},{"term":{"cleaned_text":assoc}} ] } }, "aggs" : { "wordassoc" : { "terms" : { "field" : "cleaned_text" ,"size":50} }  } }   
       elif type!="all":
         body = {"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"mentions_screen.raw":word }},{"term":{"type":type }},{"term":{"sentiment_analysis":sentiment }} ,{"term":{"cleaned_text":assoc}}] } }, "aggs" : { "wordassoc" : { "terms" : { "field" : "cleaned_text" ,"size":50} }  } } 
       else:
         body = {"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"mentions_screen_name.raw":word }},{"term":{"sentiment_analysis":sentiment }} ,{"term":{"cleaned_text":assoc}}] } }, "aggs" : { "wordassoc" : { "terms" : { "field" : "cleaned_text" ,"size":50} }  } }        
    else :
       if sentiment=="All":
           if type !="all":
                  body = {"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"screen_name.raw":word }},{"term":{"type":type }},{"term":{"cleaned_text":assoc}} ] } }, "aggs" : { "wordassoc" : { "terms" : { "field" : "cleaned_text" ,"size":50} }  } }  
           else:
                  body = {"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"screen_name.raw":word }},{"term":{"cleaned_text":assoc}} ] } }, "aggs" : { "wordassoc" : { "terms" : { "field" : "cleaned_text" ,"size":50} }  } }   
       elif type!="all":
         body = {"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"screen.raw":word }},{"term":{"type":type }},{"term":{"sentiment_analysis":sentiment }} ,{"term":{"cleaned_text":assoc}}] } }, "aggs" : { "wordassoc" : { "terms" : { "field" : "cleaned_text" ,"size":50} }  } } 
       else:
         body = {"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"screen_name.raw":word }},{"term":{"sentiment_analysis":sentiment }} ,{"term":{"cleaned_text":assoc}}] } }, "aggs" : { "wordassoc" : { "terms" : { "field" : "cleaned_text" ,"size":50} }  } } 
    s=TweetsDocument.search().from_dict(body)
    t=s.execute()
    freq_data=[]
    for item  in t.aggregations.wordassoc:
        freq_data.append([item.key,item.doc_count])
    context={"text":freq_data}
    return render(request,'word-assoc.html',context)

@compress_page
def network(request,country,min,max,top,word,skip):
    limit=1000
    if top=="top":
        body = {"size":1000,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country}}] } } } 
    elif top=="hashtags":
        body={"size":1000,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country}},{"term":{"hashtags.raw":word }}] } } }
    elif top=="mentions":
        body={"size":1000,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"mentions_screen_name.raw":word }} ] } } }
    elif top=="screen":
        body={"size":1000,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"screen_name.raw":word }} ] } } }
    elif top=="topic":
        body={"size":1000,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"topics":word }} ] } } }
    s=TweetsDocument.search().from_dict(body).sort("-timestamp_ms").params(preserve_order=True)
    coordinates_data=[]
    i=0
    for hit in s.scan():
             i+=1
             if i>int(skip)+limit:
                 break
             if i>int(skip):
                     coordinates_data.append(hit.to_dict())

    response=HttpResponse(json.dumps(coordinates_data),content_type='application/json')
    return response

@compress_page
def map_data(request,country,min,max,top,word,skip):
   # client = MongoClient('mongodb://localhost:27017/')
   # db = client.project
  #  coll = db.project_tweets
    limit=30000
    if top=="top":
    #    coordinates = coll.find({ "country" : country ,"timestamp_ms":{"$gte":int(min),"$lte":int(max)},"coordinates":{"$ne":None}}).skip(int(skip)).limit(limit)
        body = {"size":10000,"_source":["coordinates"],"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }} ] } } } 
    elif top=="topic":
    #    coordinates= coll.find({ "country" : country ,"topics":word,"timestamp_ms":{"$gte":int(min),"$lte":int(max)},"coordinates":{"$ne":None}}).skip(int(skip)).limit(limit)
        body={"size":10000,"_source":["coordinates"],"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"topics":word }} ] } } } 
    elif top=="hashtags":
     #   coordinates = coll.find({ "country" : country,"hashtags":word,"timestamp_ms":{"$gte":int(min),"$lte":int(max)},"coordinates":{"$ne":None}}).skip(int(skip)).limit(limit)
        body={"size":10000,"_source":["coordinates"],"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"hashtags.raw":word }} ] } } } 
    elif top=="mentions":
      #  coordinates = coll.find({ "country" : country,"mentions_screen_name":word,"timestamp_ms":{"$gte":int(min),"$lte":int(max)},"coordinates":{"$ne":None}}).skip(int(skip)).limit(limit) 
        body={"size":10000,"_source":["coordinates"],"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"mentions_screen_name.raw":word }} ] } } }
    elif top=="screen":
       # coordinates = coll.find({"country" : country,"screen_name":word,"timestamp_ms":{"$gte":int(min),"$lte":int(max)},"coordinates":{"$ne":None}}).skip(int(skip)).limit(limit)
        body={"size":10000,"_source":["coordinates"],"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country }},{"term":{"screen_name.raw":word }} ] } } }


    s=TweetsDocument.search().from_dict(body).sort("-timestamp_ms").params(preserve_order=True)
   # t=s.execute()
#    coordinates=coll.aggregate(pipeline)
   # client.close()
    coordinates_data=[]
    i=0
    for hit in s.scan():
             i+=1
             if i>int(skip)+limit:
                 break
             if i>int(skip):
                 coordinates_data.append([hit.coordinates[0],hit.coordinates[1]])
    response=HttpResponse(json.dumps(coordinates_data),content_type='application/json')
    return response

@compress_page
def map(request,country,min,max,top,word):
    bounds=stream.objects.get(country=country)
    coor=(bounds.bounding_box).split(",")
    context={"bounds":coor}
    return render(request,'map.html',context)
 # Function that is executed when search is made 
def search(request,country):
    if request.method=="POST":
            s = TweetsDocument.search().filter("match",  country=country).query('match', hashtags=request.POST.get('search'))
            s1 = TweetsDocument.search().filter("match",  country=country).query('match', screen_name=request.POST.get('search'))
            s2 = TweetsDocument.search().filter("match",country=country).query('match', text=request.POST.get('search'))
            s3 = TweetsDocument.search().filter("match", country=country).query('match', mentions_screen_name=request.POST.get('search'))
            s4 = TweetsDocument.search().filter("match", country=country).query('match', quoted_screen_name=request.POST.get('search'))
            s5 = TweetsDocument.search().filter("match", country=country).query('match', retweet_screen_name=request.POST.get('search'))
            user_data=[]
            hashtag_data=[]
            tweets_data=[]
            mentions_data=[]
            quoted_data=[]
            retweet_data=[]
            for hit in s1:
                   user_data.append([hit.screen_name,hit.profile_image_url_https,hit.text,hit.status_id])
            for hit in s3:
                   m=""
                   for men in hit.mentions_screen_name:
                       m+="@"
                       m+=men+" "  
                   mentions_data.append([m,hit.profile_image_url_https,hit.text,hit.status_id,hit.screen_name])
            for hit in s4:
                   q=""
                   for quo in hit.quoted_screen_name:
                       q+=quo+" "  
                   quoted_data.append([q,hit.profile_image_url_https,hit.text,hit.status_id,hit.screen_name])            
            for hit in s5:
                   r=""
                   for ret in hit.retweet_screen_name:
                       r+=ret+" "  
                   retweet_data.append([r,hit.profile_image_url_https,hit.text,hit.status_id,hit.screen_name])            
            for hit in s2:
                   tweets_data.append([hit.screen_name,hit.profile_image_url_https,hit.text,hit.status_id])
            for hit in s:
                   h=""
                   for hash in hit.hashtags: 
                           h+="#"				   
                           h+=hash+" "	
                   hashtag_data.append([h,hit.screen_name,hit.status_id,hit.text])              	 
            count=len(user_data)+len(hashtag_data)+len(tweets_data)+len(mentions_data)+len(quoted_data)+len(retweet_data)
            context={"users":user_data,
	"hashtags":hashtag_data,
	"tweets":tweets_data,
	"mentions":mentions_data,
	"quote":quoted_data,
	"retweet":retweet_data,
	"count":count
	}

    return render(request,'search.html', context)

def v(list): 
   if list[1]=="High":
       return 0
   elif list[1]=="Medium":
       return 1
   else:
       return 2
	
@compress_page
def get_viral(request,country,min,max,top,word,skip):
    topics=[]
    if top=="top":
     body={"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country}},{"exists":{"field":"topics"}} ] } },"aggs":{"topic":{"terms":{"field":"topics","size":10000},"aggs":{"min_time":{"min":{"field":"timestamp_ms"}}} }}}
    elif top=="mentions":
     body={"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country}},{"exists":{"field":"topics"}},{"term":{"mentions_screen_name.raw":word}} ] } },"aggs":{"topic":{"terms":{"field":"topics","size":10000},"aggs":{"min_time":{"min":{"field":"timestamp_ms"}}} }}}
    elif top=="hashtags":
     body={"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country}},{"exists":{"field":"topics"}},{"term":{"hashtags.raw":word}} ] } },"aggs":{"topic":{"terms":{"field":"topics","size":10000},"aggs":{"min_time":{"min":{"field":"timestamp_ms"}}} }}}
    elif top=="screen":
        body={"size":0,"query":{"bool":{"must":[{"range":{"timestamp_ms":{"gte":int(min),"lte":int(max) }}},{"term":{"country":country}},{"exists":{"field":"topics"}},{"term":{"screen_name.raw":word}} ] } },"aggs":{"topic":{"terms":{"field":"topics","size":10000},"aggs":{"min_time":{"min":{"field":"timestamp_ms"}}} }}}

    s=TweetsDocument.search().from_dict(body)
    t=s.execute()
    topics=[]
    virality=[]
    for item  in t.aggregations.topic:
            topics.append([item.key,item.doc_count,item.min_time.value])
    if len(topics):
        a=[l[1] for l in topics]
        low = np.percentile(a, 33)
        med = np.percentile(a, 66)
        for t in topics:
                if t[1] <low:
                    t[1]=0
                elif low<=t[1]<=med:
                    t[1]=1
                else:
                    t[1]=2
        virality=sorted(topics,key=lambda x:(x[2],x[1]),reverse=True)
    response=HttpResponse(json.dumps(virality[int(skip):int(skip)+20]),content_type='application/json')
    return response
@compress_page
def get_virality(request,country,min,max,top,word):
    return render(request,'virality.html')

def job():
    country_list=stream.objects.filter()
    t=time.mktime(datetime.now().date().timetuple())
    t=(int(t))*1000
    for country in country_list:
         get_topics(country.country,str(t))
         print(country.country)
         print(datetime.now())
    return

