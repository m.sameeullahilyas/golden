def get_topics(country,min):
 def ttopics(lis,min,max,country):
  dict={}
  str=re.compile("|".join(lis),flags=re.I|re.X)
  cursor=db.project_tweets.aggregate([{"$match":{"country":country,"timestamp_ms":{"$gte":int(min),"$lte":int(max)},"hashtags":{"$ne":""}}}])
  for doc in cursor:
      if len(doc['hashtags'])<=3:
          ls=[item for item in set(doc['hashtags']) if not re.search(str,item)]
          for l in ls:
              if l not in dict:
                     dict[l]=[]
              dict[l].extend([item for item in set(doc['hashtags']) if item!=l])
  for key in dict.keys():
      dict[key]=[item for item in dict[key] if dict[key].count(item)>=3]
      if len(dict[key])>0:
          dict[key]=list(set(dict[key]))
          global_topics=[]
          global_topics.extend(dict[key])
          i=0
          while i<len(global_topics):
                dct={}
                cursor=db.project_tweets.aggregate([{"$match":{"country":country,"timestamp_ms":{"$gte":int(min),"$lte":int(max)},"hashtags":{"$regex":global_topics[i]}}}])
                for doc in cursor:
                    if len(doc['hashtags'])<=3:
                          ls=[item for item in set(doc['hashtags']) if re.search(global_topics[i],item) and not re.search(str,item)]
                          for l in ls:
                              if l not in dct:
                                     dct[l]=[]
                              dct[l].extend([item for item in set(doc['hashtags']) if item!=l])

                for ky in dct.keys():
                     dct[ky]=[item for item in dct[ky] if dct[ky].count(item)>=3 ]
                     if len(dct[ky])>0:
                         dct[ky]=list(set(dct[ky]))
                         for a in dct[ky]:
                             if a not in global_topics:
                                   global_topics.append(a)
                i=i+1
          if key not in global_topics:
              topic=[key]
              topic.extend(global_topics)
              check=False
              for prev_topic in list_of_topics:
                  if set(prev_topic)&set(topic):
                      prev_topic.extend(list(set(topic).difference(set(prev_topic))))
                      check=True
                      break
              if not check:
                  list_of_topics.append(topic)
          else:
              topic=[]
              topic.extend(global_topics)
              check=False
              for prev_topic in list_of_topics:
                  if set(prev_topic)&set(topic):
                      prev_topic.extend(list(set(topic).difference(set(prev_topic))))
                      check=True
                      break
              if not check:
                  list_of_topics.append(topic)


 import re
 import itertools
 from project.models import tweets
 from pymongo import MongoClient
 from project.stopwords import getrestricted
 client = MongoClient('mongodb://localhost:27017/')
 db = client.project
 coll = db.project_tweets
 restricted=getrestricted()
 list_of_topics=[]
# l=db.project_tweets.aggregate([{"$group" : {"_id" : { "month": { "$month": "$created_at" }, "day": { "$dayOfMonth": "$created_at" }, "year": { "$year": "$created_at" } },"min":{"$min":"$timestamp_ms"},"max":{"$max":"$timestamp_ms"}}},{"$sort":{"_id.month":1,"_id.day":1}}])
# for ll in l:
#  if not ll['min']>int(min)+86400000:
#     list_of_topics=[]
 ttopics(restricted,min,int(min)+86400000,country)
 for top in list_of_topics:
         top=sorted(top)
         la=re.sub(r'[.:;?]',"", ", ".join(top))
         combinations=itertools.combinations(top, 2)
         for comb in combinations:
             tweets_topics=coll.find({"country":country,"timestamp_ms":{"$gte":int(min),"$lte":int(min)+86400000},"hashtags":{"$all":list(comb)}})
             for tweet_topic in tweets_topics:
                     t=tweets.objects.get(id=tweet_topic['id'])
                     topics=t.topics
                     topics.append(la)
                     topics=list(set(topics))
                     t.topics=topics
                     t.save()
